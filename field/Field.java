// Copyright 2015 theaigames.com (developers@theaigames.com)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


import bot.BotStarter;
import bot.Logger;
import field.Cell;

/**
 * Field class
 * 
 * Represents the playing field for one player.
 * Has some basic methods already implemented.
 * 
 * @author Jim van Eeden <jim@starapple.nl>
 */

public class Field {
	
	private int width;
	private int height;
	private Cell grid[][];
	String fieldString;
	


	public Field(int width, int height, String fieldString) {
		this.width = width;
		this.height = height;
		
		this.fieldString = fieldString;
		parse(fieldString);
	}
	
	/**
	 * Parses the input string to get a grid with Cell objects
	 * @param fieldString : input string
	 */
	private void parse(String fieldString) {
		
		this.grid = new Cell[this.width][this.height];
		
		// get the separate rows
		String[] rows = fieldString.split(";");
		for(int y=0; y < this.height; y++) {
			String[] rowCells = rows[y].split(",");
			
			// parse each cell of the row
			for(int x=0; x < this.width; x++) {
				int cellCode = Integer.parseInt(rowCells[x]);
				this.grid[x][y] = new Cell(x, y, CellType.values()[cellCode]);
			}
		}
	}
	
	
	
	public Cell getCell(int x, int y) {
		if(x < 0 || x >= this.width || y < 0 || y >= this.height)
			return null;
		return this.grid[x][y];
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public String getFieldString() {
		return fieldString;
	}
	
	public int evaluate() {
		/*
		int highestCell = highestCell();
		int numHoles = getNumHoles();
		return highestCell + numHoles;
		*/
		
		int landingHeight = BotStarter.lastLandingHeight;
		int rowsEliminated = BotStarter.lastRowsEliminated;
		int rowTransitions = getRowTransitions();
		int colTransitions = getColumnTransitions();
		int numHoles = getNumHoles();
		int wellSums = getWellSums();
		
		return evaluate(landingHeight, rowsEliminated, rowTransitions, colTransitions, numHoles, wellSums);
		
	}
	
	public int evaluate(int landingHeight, int rowsEliminated, int rowTransitions,  int colTransitions, int numHoles, int wellSums) {
		
		return -4*landingHeight +4*rowsEliminated -3*rowTransitions -9*colTransitions -8*numHoles -2*wellSums;
		
	}
	
	public int getNumHoles() {
		int holes = 0;
		for(int y = 0; y < 20; y++) {
			for(int x = 0; x < 10; x++) {
				if(getCell(x, y).isBlock() && getCell(x, y + 1) != null) {
					if(getCell(x, y+1).isEmpty()) {
						holes++;
					}
				}
			}
		}
		return holes;
	}
	
	public int findHeight(int col) {
		for(int y = 0; y < 20; y++) {
			Cell c = getCell(col, y);
			if(c.isFixed()) {
				return 20-y;
			}
		}
		return 0;
	}
	
	public int highestCell() {
		int highestSlope = 0;
		for(int x = 0; x < 10; x++) {
			int current = findHeight(x);
			if(current > highestSlope) {
				highestSlope = current;
			}
		}
		return highestSlope;
	}
	
	public int getRowsEliminated() {
		int rows = 0;
		for(int y = 0; y < 20; y++) {
			boolean didBreak = false;
			for(int x = 0; x < 10; x++) {
				Cell c = getCell(x, y);
				if(c.isBlock() == false) {
					didBreak = true;
				}
			}
			if(didBreak == false) {
				rows++;
			}
		}
		return rows;
	}
	
	public int getRowTransitions() {
		int transitions = 0;
		
		for(int y = 0; y < 20; y++) {
			CellType lastType = getCell(0, y).getState();
			for(int x = 1; x < 10; x++) {
				CellType currentType = getCell(x, y).getState();
				if(lastType == CellType.EMPTY && currentType == CellType.BLOCK || lastType == CellType.BLOCK && currentType == CellType.EMPTY) {
					transitions++;
				}
				lastType = currentType;
			}
		}
		return transitions;
	}
	
	public int getRowTransitionsTetrising() {
		int transitions = 0;
		
		for(int y = 0; y < 20; y++) {
			CellType lastType = getCell(0, y).getState();
			for(int x = 1; x < 9; x++) {
				CellType currentType = getCell(x, y).getState();
				if(lastType == CellType.EMPTY && currentType == CellType.BLOCK || lastType == CellType.BLOCK && currentType == CellType.EMPTY) {
					transitions++;
				}
				lastType = currentType;
			}
		}
		return transitions;
	}
	
	public int getColumnTransitions() {
		int transitions = 0;
		
		for(int x = 0; x < 10; x++) {
			CellType lastType = getCell(x, 0).getState();
			for(int y = 1; y < 20; y++) {
				CellType currentType = getCell(x, y).getState();
				if(lastType == CellType.EMPTY && (currentType == CellType.BLOCK || currentType == CellType.SOLID) || (lastType == CellType.BLOCK && currentType == CellType.EMPTY)) {
					transitions++;
				}
				lastType = currentType;
			}
			if(lastType == CellType.EMPTY) {
				transitions++;
			}
		}
		return transitions;
	}
	
	public int getWellSums() {
		int nr = 0;
		for(int x = 0; x < 9; x++) {
			for(int y = 0; y < 20; y++) {
				if((x == 0 || getCell(x - 1, y).isBlock()) && getCell(x + 1, y).isBlock() && getCell(x, y).isEmpty()) {
					nr++;
					for(int k = y + 1; k < 20; k++) {
						if(getCell(x, k).isEmpty()) {
							nr++;
						}
						else {
							break;
						}
					}
				}
			}
		}
		
		return nr;
	}
	
	public Field removeFullRowsAndClone() {
		ArrayList<Integer> fullRows = new ArrayList<Integer>();
		for(int y = 0; y < 20; y++) {
			boolean didBreak = false;
			for(int x = 0; x < 10; x++) {
				Cell c = getCell(x, y);
				if(c.isBlock() == false) {
					didBreak = true;
				}
			}
			if(didBreak == false) {
				fullRows.add(y);
			}
		}
		
		
		
		if(fullRows.size() > 0) {
			List<String> rows = new LinkedList<String>();
			
			for(int y = 0; y < 20; y++) {
				String s = "";
				for(int x = 0; x < 10; x++) {
					if(getCell(x, y).isBlock()) {
						s += "2";
					}
					else if(getCell(x, y).isSolid()) {
						s += "3";
					}
					else {
						s += "0";
					}
					s += ",";
				}
				s = s.substring(0, s.length() - 1);
				rows.add(s);
			}
			
			for(Integer i : fullRows) {
				rows.remove(i + 0);
				rows.add(0, "0,0,0,0,0,0,0,0,0,0");
			}

			String fieldString2 = "";
			
			for(String s : rows) {
				fieldString2 += s + ";";
			}
			
			fieldString2 = fieldString2.substring(0, fieldString2.length() - 1);		
			return new Field(10, 20, fieldString2);	
		}
		
		return new Field(10, 20, getUpdatedFieldString());
	}
	
	public String getUpdatedFieldString() {
		
		String result = "";
		
		for(int y = 0; y < 20; y++) {
			String s = "";
			for(int x = 0; x < 10; x++) {
				if(getCell(x, y).isBlock()) {
					s += "2";
				}
				else if(getCell(x, y).isSolid()) {
					s += "3";
				}
				else {
					s += "0";
				}
				s += ",";
			}
			s = s.substring(0, s.length() - 1);
			result += s;
			result += ";";
		}
		
		result = result.substring(0, result.length() - 1);
		
		return result;
	}

	
}





