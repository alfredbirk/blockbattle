// Copyright 2015 theaigames.com (developers@theaigames.com)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package field;

import java.awt.Point;
import java.util.ArrayList;

import bot.Logger;
import field.Cell;
import field.Field;
import field.ShapeType;

/**
 * Shape class
 * 
 * Represents the shapes that appear in the field.
 * Some basic methods have already been implemented, but
 * actual move actions, etc. should still be created.
 * 
 * @author Jim van Eeden <jim@starapple.nl>
 */

public class Shape {
	
	public static int[][] IRot = {{ 0, 1, 1, 1, 2, 1, 3, 1}, {2, 0, 2, 1, 2, 2, 2, 3}, {0, 2, 1, 2, 2, 2, 3, 2}, {1, 0, 1, 1, 1, 2, 1, 3}};
	public static int[][] JRot = {{0, 0, 0, 1, 1, 1, 2, 1}, {1, 0, 2, 0, 1, 1, 1, 2}, {0, 1, 1, 1, 2, 1, 2, 2}, {1, 0, 1, 1, 1, 2, 0, 2}};
	public static int[][] LRot = {{2, 0, 0, 1, 1, 1, 2, 1}, {1, 0, 1, 1, 1, 2, 2, 2}, {0, 1, 1, 1, 2, 1, 0, 2}, {0, 0, 1, 0, 1, 1, 1, 2}};
	public static int[][] ORot = {{0, 0, 1, 0, 0, 1, 1, 1}, {0, 0, 1, 0, 0, 1, 1, 1}, {0, 0, 1, 0, 0, 1, 1, 1}, {0, 0, 1, 0, 0, 1, 1, 1}};
	public static int[][] SRot = {{1, 0, 2, 0, 0, 1, 1, 1}, {1, 0, 1, 1, 2, 1, 2, 2}, {1, 1, 2, 1,  0, 2, 1, 2}, {0, 0, 0, 1, 1, 1, 1, 2}};
	public static int[][] TRot = {{1, 0, 0, 1, 1, 1, 2, 1}, {1, 0, 1, 1, 2, 1, 1, 2}, {0,  1, 1, 1, 2, 1, 1, 2}, {1, 0, 0, 1, 1, 1, 1, 2}};
	public static int[][] ZRot = {{0, 0, 1, 0, 1, 1, 2, 1}, {2, 0, 1, 1, 2, 1, 1, 2}, {0, 1, 1, 1, 1, 2, 2, 2}, {1, 0, 0, 1, 1, 1, 0, 2}};

	public ShapeType type;
	private int size;
	private Point location;
	private Cell[] blocks;
	private Field field;
	private int rotationIndex;
	private int[][] rotationArray;
	private int numRotations;
	private Point startPosition;
	
	public Shape(ShapeType type, Field field) {
		this.type = type;
		this.field = field;
		this.blocks = new Cell[4];
		
		setShape();
		location = startPosition;
		setBlockLocations();

		
	}
	
	private void setBlockLocations() {
		for(int i = 0; i < 4; i++) {
			int[] arr = rotationArray[rotationIndex];
			blocks[i].setLocation(location.x + arr[2*i], location.y + arr[2*i+1]);
		}
	}
	
	public void rotateCW() {
		rotationIndex++;
		if(rotationIndex > 3) {
			rotationIndex = 0;
		}
		//setLocation(getLeftest(), getLowest());
		setBlockLocations();
	}
	
	public void rotateCCW() {
		rotationIndex--;
		if(rotationIndex < 0) {
			rotationIndex = 3;
		}
		setBlockLocations();
	}
	
	public void setOrientation(int i) {
		rotationIndex = i;
		setBlockLocations();
	}
	
	public int getOrientation() {
		return rotationIndex;
	}
	

	
	// set shape in square box
	// creates new Cells that can be checked against the actual
	// playing field.
	private void setShape() {
		switch(this.type) {
			case I:
				rotationArray = IRot;
				rotationIndex = 0;
				numRotations = 2;
				startPosition = new Point(3, -1);
				this.blocks[0] = new Cell();
				this.blocks[1] = new Cell();
				this.blocks[2] = new Cell();
				this.blocks[3] = new Cell();
				break;
			case J:
				rotationArray = JRot;
				rotationIndex = 0;
				numRotations = 4;
				startPosition = new Point(3, -1);
				this.blocks[0] = new Cell();
				this.blocks[1] = new Cell();
				this.blocks[2] = new Cell();
				this.blocks[3] = new Cell();
				break;
			case L:
				rotationArray = LRot;
				rotationIndex = 0;
				numRotations = 4;
				startPosition = new Point(3, -1);
				this.blocks[0] = new Cell();
				this.blocks[1] = new Cell();
				this.blocks[2] = new Cell();
				this.blocks[3] = new Cell();
				break;
			case O:
				rotationArray = ORot;
				rotationIndex = 0;
				numRotations = 1;
				startPosition = new Point(4, -1);
				this.blocks[0] = new Cell();
				this.blocks[1] = new Cell();
				this.blocks[2] = new Cell();
				this.blocks[3] = new Cell();
				break;
			case S:
				rotationArray = SRot;
				rotationIndex = 0;
				numRotations = 2;
				startPosition = new Point(3, -1);
				this.blocks[0] = new Cell();
				this.blocks[1] = new Cell();
				this.blocks[2] = new Cell();
				this.blocks[3] = new Cell();
				break;
			case T:
				
				rotationArray = TRot;
				rotationIndex = 0;
				numRotations = 4;
				startPosition = new Point(3, -1);
				this.blocks[0] = new Cell();
				this.blocks[1] = new Cell();
				this.blocks[2] = new Cell();
				this.blocks[3] = new Cell();
				break;
			case Z:
				rotationArray = ZRot;
				rotationIndex = 0;
				numRotations = 2;
				startPosition = new Point(3, -1);
				this.blocks[0] = new Cell();
				this.blocks[1] = new Cell();
				this.blocks[2] = new Cell();
				this.blocks[3] = new Cell();
				break;
		}
		
		// set type to SHAPE
		for(int i=0; i < blocks.length; i++) {
			this.blocks[i].setShape();
		}
	}
		
	private Cell[][] initializeShape() {
		Cell[][] newShape = new Cell[size][size];
		for(int y=0; y < this.size; y++) {
			for(int x=0; x < this.size; x++) {
				newShape[x][y] = new Cell();
			}
		}
		return newShape;
	}
	
	
	public void setLocation(int x, int y) {
		this.location = new Point(x, y);
		setBlockLocations();
	}
	
	public Cell[] getBlocks() {
		return this.blocks;
	}
	
	public Point getLocation() {
		return this.location;
	}
	
	public ShapeType getType() {
		return this.type;
	}
	
	public boolean collision() {
		for(Cell c: blocks) {
			try {
				if(c.isOutOfBoundaries(field) || c.hasCollision(field)) {
					return true;
				}
			}
			catch(Exception e) {
				return true;
			}

		}
		return false;
	}
	
	
	public boolean canMoveDown() {
		moveDown();
		if(collision()) {
			moveUp();
			return false;
		}
		else {
			moveUp();
			return true;
		}
		
	}
	
	public boolean canMoveLeft() {
		moveLeft();
		if(collision()) {
			moveRight();
			return false;
		}
		else {
			moveRight();
			return true;
		}
		
	}
	
	
	public void moveLeft() {
		location.x -= 1;
		setBlockLocations();
	}
	
	public void moveRight() {
		location.x += 1;
		setBlockLocations();
	}
	
	public void moveDown() {
		location.y += 1;
		setBlockLocations();
	}
	
	public void moveUp() {
		location.y -= 1;
		setBlockLocations();
	}
	
	
	public int getLeftest() {
		
		int min = Integer.MAX_VALUE;
		
		for(Cell c: blocks) {
			int x = c.getLocation().x;
			if(x < min) {
				min = x;
			}
		}
		
		return min;
	}
	
	public int getRightest() {
		
		int max = 0;
		
		for(Cell c: blocks) {
			int x = c.getLocation().x;
			if(x > max) {
				max = x;
			}
		}
		
		return max;
	}
	
	public int getHighest() {
		int max = 0;
		
		for(Cell c: blocks) {
			int y = c.getLocation().y;
			if(y > max) {
				max = y;
			}
		}
		
		return max;
	}
	
	public int getLowest() {
		
		int min = Integer.MAX_VALUE;
		
		for(Cell c: blocks) {
			int y = c.getLocation().y;
			if(y < min) {
				min = y;
			}
		}
		
		return min;
	}
	
	public int getPieceHeight() {
		return getHighest() - getLowest() + 1;
	}
	
	public int getHighestPossible() {
		while(true) {
			moveDown();
			if(collision()) {
				moveUp();
				break;
			}
		}

		int highest = getHighest();
		
		return highest;
	}
	
	public void addToField() {
		for(Cell c: blocks) {
			field.getCell(c.getLocation().x, c.getLocation().y).setBlock();
		}
	}
	
	public void removeFromField() {
		for(Cell c: blocks) {
			field.getCell(c.getLocation().x, c.getLocation().y).setEmpty();
		}
	}
	
	public void setField(Field f) {
		this.field = f;
	}
	
	public int getNumRotations() {
		return numRotations;
	}

	public Field getField() {
		return field;
	}

	public ArrayList<Point> getCellLocations() {
		ArrayList<Point> a = new ArrayList<Point>();
		
		for(Cell c : blocks) {
			a.add(c.getLocation());
		}
		
		return a;
		
	}
}








