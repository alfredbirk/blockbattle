// Copyright 2015 theaigames.com (developers@theaigames.com)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package bot;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import field.Field;
import field.Shape;
import field.ShapeType;
import moves.MoveType;

/**
 * BotStarter class
 * 
 * This class is where the main logic should be. Implement getMoves() to
 * return something better than random moves.
 * 
 * @author Jim van Eeden <jim@starapple.nl>
 */

public class BotStarter {
	
	public static int lastLandingHeight;
	public static int lastRowsEliminated;
	public static int lastRowsEliminated2;

	public BotStarter() {}
	
	

	
	/**
	 * Returns a random amount of random moves
	 * @param state : current state of the bot
	 * @param timeout : time to respond
	 * @return : a list of moves to execute
	 */
	public ArrayList<MoveType> getMoves(BotState state, long timeout) {
		
		long totalTime = System.nanoTime();
		
		Field field = state.getMyField();
		Field field2 = null;



		ArrayList<MoveType> moves = new ArrayList<MoveType>();

		
		int bestEval = -1000000;
		int bestX = 0;;
		int bestOrientation = 0;
		int bestLandingHeight = 0;
		int bestRowsEliminated = 0;
		int bestRowTransitions = 0;
		int bestColTransitions = 0;
		int bestNumHoles = 0;
		int bestWellSums = 0;
		
		int bestX2 = 0;
		int bestOrientation2 = 0;
		int bestLandingHeight2 = 0;
		
		
		ArrayList<Integer> evals = new ArrayList<Integer>();
		
		Shape shape = new Shape(state.getCurrentShape(), field);
		int shapeX = shape.getLeftest();
		int shapeOrientation = 0;
		int numRotations = shape.getNumRotations();
		
		Shape shape2 = new Shape(state.getNextShape(), field2);
		int shapeX2 = shape2.getLeftest();
		int shapeOrientation2 = 0;
		int numRotations2 = shape2.getNumRotations();
		
		int originalNumHoles = field.getNumHoles();
		int holes1 = 0;
		
		int cols;
		int highestCell = field.highestCell();
		boolean tetrising;
		
		if(originalNumHoles == 0 && highestCell < 12) {
			tetrising = true;
			cols = 9;
		}
		else {
			tetrising = false;
			cols = 10;
		}
		
		for(int x = 0; x < 10; x++) {
			for(int dir = 0; dir < numRotations; dir++) {
				
				shape.setOrientation(dir);
				shape.setLocation(x - (shape.getLeftest() - shape.getLocation().x), -1);
				if(shape.collision()) {
					continue;
				}
				
				shape.getHighestPossible();
				
				if(shape.getLowest() < 0) {
					continue;
				}
				
				shape.addToField();
				lastRowsEliminated = field.getRowsEliminated();
				holes1 = field.getNumHoles();
				
				Field fieldClone = null;
				
				if(lastRowsEliminated > 0) {
					fieldClone = field.removeFullRowsAndClone();					
				}
				else {
					fieldClone = field;
				}
				
				field2 = fieldClone;
				shape2.setField(field2);
				
				for(int x2 = 0; x2 < 10; x2++) {
					for(int dir2 = 0; dir2 < numRotations2; dir2++) {
						
						
						
						shape2.setOrientation(dir2);
						shape2.setLocation(x2 - (shape2.getLeftest() - shape2.getLocation().x), -1);
						if(shape2.collision()) {
							continue;
						}
						
						shape2.getHighestPossible();
						
						if(shape2.getLowest() < 0) {
							continue;
						}

						try{
							shape2.addToField();							
						}
						catch(Exception e) {
							Logger.log("FEIL HER:");
							Logger.log("Shape2 leftest: " + shape2.getLeftest());
							Logger.log("Shape2 x2: " + x2);
							Logger.log("Shape2 Field: " + shape2.getField());
							Logger.log("" + shape2.getCellLocations().toString());
							Logger.log("Shape2 orientation: " + shape2.getOrientation());
						}

						
						lastLandingHeight = 19 - (shape.getHighest()) + shape.getPieceHeight();
						lastRowsEliminated2 = field2.getRowsEliminated();
					
						
						Field fieldClone2 = null;
						
						if(lastRowsEliminated2 > 0) {
							fieldClone2 = field2.removeFullRowsAndClone();					
						}
						else {
							fieldClone2 = field2;
						}
						
						int lastRowTransitions;
						
						if(tetrising) {
							lastRowTransitions = fieldClone2.getRowTransitionsTetrising();
						}
						else {
							lastRowTransitions = fieldClone2.getRowTransitions();
						}
						 
						int lastColTransitions = fieldClone2.getColumnTransitions();
						int lastNumHoles = fieldClone2.getNumHoles();
						int lastWellSums = fieldClone2.getWellSums();
						
						int current = fieldClone2.evaluate(lastLandingHeight, lastRowsEliminated + lastRowsEliminated2, lastRowTransitions, lastColTransitions, lastNumHoles, lastWellSums);
						
						if(tetrising) {
							if(shape.getRightest() == 9 && originalNumHoles == 0 && lastRowsEliminated != 4) {
								current -= 100;
							}
							
							if(shape2.getRightest() == 9 && holes1 == 0 && lastRowsEliminated2 != 4) {
								current -= 100;
							}
						}

						
						if(current > bestEval) {
							bestEval = current;
							bestX = x;
							bestOrientation = dir;
							bestLandingHeight2 = lastLandingHeight;
							bestRowsEliminated = lastRowsEliminated;
							bestRowTransitions = lastRowTransitions;
							bestColTransitions = lastColTransitions;
							bestNumHoles = lastNumHoles;
							bestWellSums = lastWellSums;
							
							bestX2 = x2;
							bestOrientation2 = dir2;

						}
						evals.add(current);
						shape2.removeFromField();
						
					}
				}
				
				shape.removeFromField();
			}

			
		}
		
		Logger.log("Evals: " + evals.size());
		
		Logger.log("Evaluation: " + bestEval);
		Logger.log("Highest cell: " + highestCell);
		Logger.log("Best x: " + bestX);
		Logger.log("Best Orientation: " + bestOrientation);
		Logger.log("Best x2: " + bestX2);
		Logger.log("Best Orientation2: " + bestOrientation2);
		Logger.log("Landing Height: " + bestLandingHeight2);
		Logger.log("Rows eliminated: " + bestRowsEliminated);
		Logger.log("Row transitions: " + bestRowTransitions);
		Logger.log("Col transitions: " + bestColTransitions);
		Logger.log("Holes: " + bestNumHoles);
		Logger.log("Well sums: " + bestWellSums);
		Logger.log("Best field:");
		
		
		//Recreate best field
		shape.setOrientation(bestOrientation);
		shape.setLocation(bestX - (shape.getLeftest() - shape.getLocation().x), -1);
		shape.getHighestPossible();
		shape.addToField();
		
		Field bestField = field.removeFullRowsAndClone();	
		
		shape2.setField(bestField);
		shape2.setOrientation(bestOrientation2);
		shape2.setLocation(bestX2 - (shape2.getLeftest() - shape2.getLocation().x), -1);
		shape2.getHighestPossible();
		shape2.addToField();
		
		Field bestField2 = bestField.removeFullRowsAndClone();	
		
		String bestFieldString = bestField2.getUpdatedFieldString();
		
		for(String s : bestFieldString.split(";")) {
			Logger.log(s);			
		}

		shape.removeFromField();
		shape2.removeFromField();
		
		while(shapeOrientation < bestOrientation) {
			moves.add(MoveType.TURNRIGHT);
			shapeOrientation++;
		}
		
		shape.setOrientation(bestOrientation);
		shapeX += shape.getLeftest() - shape.getLocation().x;
		
		while(shapeX > bestX) {
			moves.add(MoveType.LEFT);
			shapeX--;
		}
		while(shapeX < bestX) {
			moves.add(MoveType.RIGHT);
			shapeX++;
		}
		
		moves.add(MoveType.DOWN);
		
		long totalEnd = System.nanoTime() - totalTime;
		totalEnd = totalEnd / 1000000;
		Logger.log("Total time: " + totalEnd);
		
		return moves;
	}
	
	public static void main(String[] args)
	{
		BotParser parser = new BotParser(new BotStarter());
		parser.run();
	}
	

}
