package bot;

public class Logger {
	
	public static void log(String s) {
		newLine();
		System.err.printf(s);
	}
	
	public static void newLine() {
		newLine(1);
	}
	
	public static void newLine(int n) {
		for(int i = 0; i < n; i++) {
			System.err.printf("\n");			
		}
	}

}
